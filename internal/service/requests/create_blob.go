package requests

import (
	"encoding/json"
	"errors"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/slbmax/blobservice/internal/data"
	"gitlab.com/slbmax/blobservice/resources"
	"net/http"
)

func NewCreateBlobRequest(r *http.Request) (resources.Blob, error) {
	var req resources.BlobResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return req.Data, errors.New("failed to unmarshal")
	}
	return req.Data, ValidateCreateBlobRequest(req.Data)
}

func ValidateCreateBlobRequest(r resources.Blob) error {
	return validation.Errors{
		"/data/attributes/value":    validation.Validate(&r.Attributes.Value, validation.Required),
		"/data/relationships/owner": validation.Validate(&r.Relationships.Owner, validation.Required),
	}.Filter()
}

func Blob(r resources.Blob) (*data.Blob, error) {
	return &data.Blob{
		ID:      r.ID,
		Value:   r.Attributes.Value,
		OwnerId: r.Relationships.Owner,
	}, nil
}
