package service

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/slbmax/blobservice/internal/config"
	"gitlab.com/slbmax/blobservice/internal/data/postgres"
	"gitlab.com/slbmax/blobservice/internal/service/handlers"
)

func (s *service) router(cfg config.Config) chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(postgres.NewBlobs(cfg.DB())),
			handlers.CtxOwnerQ(postgres.NewOwners(cfg.DB())),
		),
	)
	r.Route("/integrations/blobservice", func(r chi.Router) {
		r.Post("/blobs", handlers.CreateBlob)
		r.Get("/blobs", handlers.GetBlobsList)
		r.Delete("/blobs/{id}", handlers.DeleteBlob)
		r.Get("/blobs/{id}", handlers.GetBlob)
	})
	return r
}
