package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/slbmax/blobservice/internal/data"
	"gitlab.com/slbmax/blobservice/internal/service/requests"
	"net/http"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewDeleteBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("invalid request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	var blob *data.Blob
	blob, err = BlobQ(r).GetBlobById(request.BlobID)
	if err != nil {
		Log(r).WithError(err).Info("no such blob to delete")
		ape.RenderErr(w, problems.NotFound())
		return
	}
	err = BlobQ(r).DeleteBlob(blob.ID)
	if err != nil {
		Log(r).WithError(err).Error("blob deletion failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
