package handlers

import (
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/slbmax/blobservice/internal/service/requests"
	"gitlab.com/slbmax/blobservice/resources"
	"net/http"
)

func GetBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("invalid request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	blob, err := BlobQ(r).GetBlobById(request.BlobID)
	if err != nil {
		Log(r).WithError(err).Error("failed to get blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if blob == nil {
		Log(r).WithError(err).Error("no such blob")
		ape.RenderErr(w, problems.NotFound())
		return
	}
	response := resources.BlobResponse{
		Data: NewBlob(blob),
	}
	ape.Render(w, &response)
	w.WriteHeader(http.StatusOK)
}
