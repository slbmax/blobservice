package main

import (
	"os"

	"gitlab.com/slbmax/blobservice/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
