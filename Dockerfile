FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/blobservice
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/blobservice /go/src/blobservice


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/blobservice /usr/local/bin/blobservice
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["blobservice"]
